import express, { Express, Request, Response } from 'express';

import indexRouter from './routes/index';

const app: Express = express();
const port: Number = 3002;

app.use('/', indexRouter);

app.listen(port, () => {
    console.log('[server]: The server is running at http://localhost:' + port);
});
