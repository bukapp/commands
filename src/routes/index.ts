import express, { Express, Request, Response, Router, NextFunction } from 'express';

var router: Router = express.Router()

/* GET home page */
router.get('/', function(req: Request, res: Response, next: NextFunction) {
    res.send('Commands service homepage')
});

export default router;
